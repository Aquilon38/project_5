Liens vers la dernière analyse Codacy : https://app.codacy.com/manual/jonathanmadelon/project_5/dashboard?bid=14843252&token=PJT1OcbGHD85UWD
Issues BitBucket : https://bitbucket.org/Aquilon38/project_5/issues?status=new&status=open
Issues Trello : https://trello.com/b/QsOoNdlC/projet-5-blogpost

Pour les tests :
Compte subscriber Identifiant : jonmad@gmail.com   Mot de passe : chouchou
Compte admin Identifiant : jonadmin@gmail.com   Mot de passe : chouchou


Installation du projet:

En local

1.Veuillez installer une plateforme de développement web (sous mac -> mamp)
2.Assigner le document 'blog' comme racine
3.Veuillez installer une application Web de gestion pour les systèmes de gestion de base de données MySQL (phpMyAdmin par exemple)
4.Importez les tables de la base de donnée figurant dans le fichier joint bdd.txt
5.Veuillez modifier, s'il y a lieu, les instructions de connexion à la base de donnée en tant que constante (define), à blog/config/config.php


Sur internet

1.Après avoir choisi l'hébergeur, et le nom de domaine, veuillez conservez vos identifiants et mots de passe de connexion ainsi que vers votre base de donnée.
2.A l'aide d'un client FTP/FTPS (FileZilla), vous pouvez transferer la totalité du fichier blog dans le répertoire attitré de votre site internet.
3.Importez les tables de la base de donnée figurant dans le fichier joint bdd.txt
4.Veuillez modifier, s'il y a lieu, les instructions de connexion à la base de donnée en tant que constante (define), à blog/config/config.php