<?php

    class SubscriberManager extends Dbconnect{
        
        public function add(Subscriber $subscriber)
        {
            try{
            $add = $this->db->prepare('
                INSERT INTO subscribers (mail, firstName, lastName, password, type) 
                VALUES (:mail, :firstName, :lastName, :password, :type)
                ');
            $add->bindvalue(':mail', $subscriber->mail());
            $add->bindvalue(':firstName', $subscriber->firstName());
            $add->bindvalue(':lastName', $subscriber->lastName());
            $add->bindvalue(':password', $subscriber->password());
            $add->bindvalue(':type', $subscriber->type());
            $add->execute();
            $id = $this->db->lastInsertId();
            $subscriber->setIdSubscriber($id);
            return $subscriber;
            }
            
            catch (Exception $ex){
                if($ex->getCode()==23000){
                    throw new Exception ('Cette adresse e-mail est déjà existente');
                }
            }
        }
            
        public function read($id)
        {
            $read = $this->db->prepare('
                SELECT mail, firstName, lastName, idSubscriber, date, type, validation, password FROM subscribers WHERE idSubscriber=:idSubscriber'
                    );
            $read->bindValue(':idSubscriber', $id);
            $read->execute();
            $donnees = $read->fetch();
            if($donnees!=NULL){
                $subscriber = new Subscriber($donnees);
                return $subscriber;   
            }
            return NULL;
        }
        
        public function readByMail($mail)
        {
            $read = $this->db->prepare('
                SELECT mail, firstName, lastName, idSubscriber, date, type, validation, password FROM subscribers WHERE mail=:mail'
                    );
            $read->bindValue(':mail', $mail);
            $read->execute();
            $donnees = $read->fetch();
            if($donnees!=NULL){
                $subscriber = new Subscriber($donnees);
                return $subscriber;
            }
            return NULL;
        }
        
        
        public function findAll($orderBy = null, $AscOrDesc = null) {
        
            $query = 'SELECT mail, firstName, lastName, idSubscriber, date, type, validation, password FROM subscribers';
    
            if ($orderBy != null){
                   $query .= ' ORDER BY '.$orderBy;
            }

            $query.=' '.$AscOrDesc;
            
            $findAll = $this->db->query($query);
           
            while ($donnees = $findAll->fetch())
            {
                $subscriber = new Subscriber($donnees);
                $subscribers[] = $subscriber;  
            }
            return $subscribers;
        }
        
        
        public function modify(Subscriber $subscriber)
        {
            $modify = $this->db->prepare('
                UPDATE subscribers SET mail = :mail, firstName = :firstName, lastName = :lastName, date = :date, password = :password, type = :type, validation = :validation WHERE idSubscriber = :idSubscriber
                    '
                    );
            $modify->bindvalue(':mail', $subscriber->mail());
            $modify->bindvalue(':firstName', $subscriber->firstName());
            $modify->bindvalue(':lastName', $subscriber->lastName());
            $modify->bindvalue(':date', $subscriber->date());
            $modify->bindvalue(':password', $subscriber->password());
            $modify->bindvalue(':idSubscriber', $subscriber->idSubscriber()); 
            $modify->bindvalue(':type', $subscriber->type());
            $modify->bindvalue(':validation', $subscriber->validation());
            $modify->execute();
        }
        
        public function delete(Subscriber $subscriber)
        {
            $delete = $this->db->prepare('
            DELETE FROM subscribers WHERE idSubscriber = :idSubscriber'
                    );
            $delete->bindvalue(':idSubscriber', $subscriber->idSubscriber());
            $delete->execute();
        }
    }