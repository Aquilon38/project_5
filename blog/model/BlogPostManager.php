<?php

    class BlogPostManager extends Dbconnect{
        
        public function add(BlogPost $post)
        {
            $add = $this->db->prepare('
                INSERT INTO blog_posts (idSubscriber, chapo, content, date, title) 
                VALUES (:idSubscriber, :chapo, :content, :date, :title)
                ');
            $add->bindvalue(':idSubscriber', $post->idSubscriber());          
            $add->bindvalue(':chapo', $post->chapo());
            $add->bindvalue(':content', $post->content());
            $add->bindvalue(':date', $post->date());
            $add->bindvalue(':title', $post->title());
            $add->execute();
            $id = $this->db->lastInsertId();
            $post->setIdBlogPost($id);
            return $post;
        }
        
        public function read($id)
        {
            $read = $this->db->prepare('
                SELECT idSubscriber, idBlogPost, chapo, content, date, lastModificationDate, title FROM blog_posts WHERE idBlogPost=:idBlogPost'
                );
            $read->bindvalue(':idBlogPost', $id);
            $read->execute();
            $donnees = $read->fetch();
            
            if($donnees == false){
                throw new Exception('Ce blogpost n\'existe pas');
            }
            
            $bp = new BlogPost();
            $bp->hydrate($donnees);

            return $bp;     
        }
        
        
        //Création de la méthode optionnelle
        
        public function findAll($orderBy = null, $AscOrDesc = ' ASC') 
        {
            $query = 'SELECT idSubscriber, idBlogPost, chapo, content, date, lastModificationDate, title FROM blog_posts';
    
            if ($orderBy != null){
                   $query .= ' ORDER BY '.$orderBy;
            }

            $query.=' '.$AscOrDesc;
           
            $findAll = $this->db->query($query);
           
            $blogposts=NULL;
            
            while($data = $findAll->fetch()){
                $blogpost = new BlogPost($data);
                $blogposts[]=$blogpost;
            }

            if($blogposts==NULL){
                $_SESSION['errors'][]="Il n'y a aucun Blog Post pour le moment";
            }
            return $blogposts;
        }
        
        
        
        public function findFromIdSubscriber($idSubscriber)
        {
            $read = $this->db->prepare('
                SELECT idSubscriber, idBlogPost, chapo, content, date, lastModificationDate, title FROM blog_posts WHERE idSubscriber=:idSubscriber'
                );
            $read->bindvalue(':idSubscriber', $idSubscriber);
            $read->execute();
            
            $blogposts = NULL;
            
            while ($donnees=$read->fetch())
            {
                $blogpost=new BlogPost($donnees);
                $blogposts[] = $blogpost;
            }
            return $blogposts;
        }
                
        public function modify(BlogPost $post)
        {
            $modify = $this->db->prepare('
                UPDATE blog_posts SET idSubscriber = :idSubscriber, chapo = :chapo, content = :content, lastModificationDate = :lastModificationDate, title = :title WHERE idBlogPost = :idBlogPost
                    '
                    );
            $modify->bindvalue(':idSubscriber', $post->idSubscriber());
            $modify->bindvalue(':chapo', $post->chapo());
            $modify->bindvalue(':content', $post->content());
            $modify->bindvalue(':idBlogPost', $post->idBlogPost());  
            $modify->bindvalue(':lastModificationDate', $post->lastModificationDate());
            $modify->bindvalue(':title', $post->title());
            $modify->execute();
        }
                        
        public function delete(BlogPost $post)
        {
            $delete = $this->db->prepare('
                DELETE FROM blog_posts WHERE idBlogPost = :idBlogPost'
                    );
            $delete->bindvalue(':idBlogPost', $post->idBlogPost());
            $delete->execute();
        }        
    }