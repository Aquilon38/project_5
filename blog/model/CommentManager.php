<?php

    class CommentManager extends Dbconnect{
        
        public function add(Comment $comment)
        {
            $add = $this->db->prepare('
                INSERT INTO comments (idSubscriber, content, idBlogPost) 
                VALUES (:idSubscriber, :content, :idBlogPost)
                ');
            $add->bindvalue(':idSubscriber', $comment->idSubscriber());
            $add->bindvalue(':content', $comment->content());
            $add->bindvalue(':idBlogPost', $comment->idBlogPost());
            $add->execute();
            $id = $this->db->lastInsertId();
            $comment->setIdComment($id);
            return $comment;
        }
        
        public function read($id_comment)
        {
            $read = $this->db->prepare('
                SELECT idSubscriber, idComment, content, idBlogPost, idState, date FROM comments WHERE idComment=:idComment'
                );
            $read->bindvalue(':idComment', $id_comment);
            $read->execute();
            $donnees=$read->fetch();
            
            if(empty($donnees)){
                throw new Exception("Le commentaire a bien été éffacé");
            }            

            else{
                $comment=new Comment($donnees);            
                return $comment;
            }
        }
        
        public function findFromIdSubscriber($idSubscriber)
        {
            $read = $this->db->prepare('
                SELECT idSubscriber, idComment, content, idBlogPost, idState, date FROM comments WHERE idSubscriber=:idSubscriber'
                );
            $read->bindvalue(':idSubscriber', $idSubscriber);
            $read->execute();
            
            $comments = NULL;
            
            while ($donnees=$read->fetch())
            {
                $comment=new Comment();
                $comment->hydrate($donnees);
                $comments[] = $comment;
            }
            return $comments;
        }
        
        
        public function findFromIdBlogPost($idBlogPost)
        {
            $read = $this->db->prepare('
                SELECT idSubscriber, idComment, content, idBlogPost, idState, date FROM comments WHERE idBlogPost=:idBlogPost'
                );
            $read->bindvalue(':idBlogPost', $idBlogPost);
            $read->execute();
            
            $comments = array();
            
            while ($donnees=$read->fetch())
            {
                $comment=new Comment($donnees);
                $comments[] = $comment;
            }
            
            return $comments;
        }
        
        
        public function findFromIdBlogPostForValidateComments($idBlogPost)
        {
            $read = $this->db->prepare('
                SELECT idSubscriber, idComment, content, idBlogPost, idState, date FROM comments WHERE idBlogPost=:idBlogPost AND idState = 1'
                );
            $read->bindvalue(':idBlogPost', $idBlogPost);
            $read->execute();
            
            $comments = array();
            
            while ($donnees=$read->fetch())
            {
                $comment=new Comment($donnees);
                $comments[] = $comment;
            }
            
            return $comments;
        }
                

        public function findFromIdState($idState)
        {
            $read = $this->db->prepare('
                SELECT idSubscriber, idComment, content, idBlogPost, idState, date FROM comments WHERE idState=:idState'
                );
            $read->bindvalue(':idState', $idState);
            $read->execute();
            
            $comments = array();
            
            while ($donnees=$read->fetch())
            {
                $comment=new Comment($donnees);
                $comments[] = $comment;
            }
            
            return $comments;
        }
        
        public function modify(Comment $comment)
        {
            $modify = $this->db->prepare('
                UPDATE comments SET content = :content, idState = :idState, date = :date WHERE idComment = :idComment
                    '
                    );
            $modify->bindvalue(':idComment', $comment->idComment());
            $modify->bindvalue(':content', $comment->content());
            $modify->bindvalue(':idState', $comment->idState());
            $modify->bindvalue(':date', $comment->date());
            $modify->execute();
        }
        
        public function findAll($orderBy = null, $AscOrDesc = null) 
        {
        
            $query = 'SELECT idSubscriber, idComment, content, idBlogPost, idState, date FROM comments';
    
            if ($orderBy != null){
                   $query .= ' ORDER BY '.$orderBy;
            }

            $query.=' '.$AscOrDesc;
            
            $findAll = $this->db->query($query);
           
            while ($donnees = $findAll->fetch())
            {
                $comment = new Comment($donnees);
                $comments[] = $comment;  
            }
            
            return $comments;
        }
        
        public function delete(Comment $comment)
        {
            $delete = $this->db->prepare('
                DELETE FROM comments WHERE idComment = :idComment'
                    );
            $delete->bindvalue(':idComment', $comment->idComment());
            $delete->execute();
        }
        
        public function commentValidationCourse()
        {
            $query = $this->db->query('
                SELECT * FROM comments WHERE idState=2;
                    '
                    );
            
            $comments=NULL;
            
            while($data = $query->fetch())
            {
                $comment = new Comment($data);
                $comments[] = $comment;
            }
            
            return $comments;
        }
        
    }