<?php
    class Subscriber extends AbstractEntity{
    
    private $idSubscriber;
    private $firstName;
    private $lastName;
    private $mail;
    private $password;
    private $date;
    private $type;
    private $validation;

    public function idSubscriber()
    {
        return $this->idSubscriber;
    }
  
    public function firstName()
    {
        return $this->firstName;
    }
  
    public function lastName()
    {
        return $this->lastName;
    }
  
    public function mail()
    {
        return $this->mail;
    }
  
    public function password()
    {
        return $this->password;
    }
  
    public function date()
    {
        if(!empty($this->date)){
            return $this->date->format('Y-m-d H:i:s');
        }
    }
    
    public function dateDisplay()
    {
        $datefmt = new IntlDateFormatter('fr_FR', NULL, NULL, NULL, NULL, 'EEEE dd LLLL à HH:mm:ss');
        if(!empty($this->date)){
            return $datefmt->format(new DateTime($this->date()));
        }
    }

    public function type()
    {
        return $this->type;
    }
    
    public function validation()
    {
        return $this->validation;
    }

    
    // Liste des setters
  
    public function setIdSubscriber($id)
    {
        $id = (int) $id;

        if ($id > 0)
        {
            $this->idSubscriber = $id;
            return $this;
        }
        
        else
        {
            throw new Exception('identifiant non toléré');
        }
    }
  
    public function setFirstName($firstName)
    {
        if (is_string($firstName) && strlen($firstName) <= 30 && strlen($firstName) >= 1 && !empty(trim($firstName)))
        {
            $this->firstName = $firstName;
            return $this;
        }
        
        else
        {
            throw new Exception('Votre prénom - Contenu incorrect');
        }
    }
  
    public function setLastName($lastName)
    {
        if (is_string($lastName) && strlen($lastName) <= 30 && strlen($lastName) >= 1 && !empty(trim($lastName)))
        {
            $this->lastName = $lastName;
            return $this;
        }
        
        else
        {
            throw new Exception('La rédaction de votre nom n\'est pas conforme');
        }
    }
  
    public function setMail($mail)
    {        
        if (preg_match("#^[\w][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$#", $mail))
        {
            $this->mail = $mail;
            return $this;
        }
        
        else
        {
            throw new Exception('Adresse mail non valide');
        }
    }
  
    public function setPassword($password)
    {
        if (is_string($password)&& strlen($password)==60)
        {
            $this->password = $password;
            return $this;
        }

        else
        {
            throw new Exception('Mot de passe non valide');
        }
    }
    
  
    public function setDate($date, $format = 'Y-m-d H:i:s')
    {     
        if(is_a($date, 'DateTime'))
        {
            $this->date=$date;
            return $this;
        }

        else{
            $this->date=DateTime::createFromFormat($format, $date);
            return $this;
        }
    }
  
    public function setType($type)
    {
        $type = (int) $type;

        if ($type===1||$type===2)
        {
            $this->type = $type;
            return $this;
        }
        
        else
        {
            throw new Exception('Type non valide');
        }
    }
    
    public function setValidation($validation)
    {
        $validation = (int) $validation;
        
        if ($validation==0||$validation==1)
        {
            $this->validation = $validation;
            return $this;
        }
        
        else
        {
            throw new Exception('Conformité de validation non respectée');
        }
    }
    
    //methode test
    public function display(){
        //Affiche les données du subscriber
        echo '<b>IdSubscriber : </b>'.$this->idSubscriber().'<br />';
        echo '<b>FirstName : </b>'.htmlspecialchars($this->firstName()).'<br />';
        echo '<b>LastName : </b>'.htmlspecialchars($this->lastName()).'<br />';
        echo '<b>Mail : </b>'.htmlspecialchars($this->mail()).'<br />';
        echo '<b>Password : </b>'.htmlspecialchars($this->password()).'<br />';
        echo '<b>Date : </b>'.$this->date().'<br />';
        echo '<b>Type : </b>'.$this->type().'<br />';
        echo '<b>Validation : </b>'.$this->validation().'<br />';
    }
    
    
    //Récupération FK class comments
    
    public function getBlogPosts(){
        
        $subscriber = $this->idSubscriber();
        $subscriberManager = new BlogPostManager();
        $listSubscriber = $subscriberManager->findFromIdSubscriber($subscriber);
        return $listSubscriber;
    }   
    //
        public function getComments()
    {
        $subscriber = $this->idSubscriber();
        $commentManager = new CommentManager();
        $listcomments = $commentManager->findFromIdSubscriber($subscriber);
        return $listcomments;
    }
    
        public function passwordToHash($connectionPassword)
    {
        $hash = password_hash($connectionPassword, PASSWORD_DEFAULT);
        return $hash;
    }
    
        public function checkPassWord($connectionPassword, $hash)
    {
        //Retourne bool
        $verification = password_verify($connectionPassword, $hash);
        return $verification;
    }
    
    public function getFullName(){
        $name = $this->lastName();
        $firstname = $this->firstName();
        return $firstname." ".$name;
    }
      
}