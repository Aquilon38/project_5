<?php

class Dbconnect
{
    protected static $staticDb=NULL;
    protected $db;


    public function __construct()
    {
        if(self::$staticDb==NULL){
            self::$staticDb = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PASSWORD);
            self::$staticDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        $this->db=self::$staticDb;
    }
}