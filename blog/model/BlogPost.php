<?php

class BlogPost extends AbstractEntity{
    
    private $idSubscriber;
    private $idBlogPost;
    private $content;
    private $chapo;
    private $title;
    private $date;
    private $lastModificationDate;

    
  
    // Liste des getters

    public function idSubscriber()
    {
        return $this->idSubscriber;
    }

    public function idBlogPost()
    {
        return $this->idBlogPost;
    }

    public function content()
    {
        return $this->content;
    }
    
    public function title()
    {
        return $this->title;
    }

    public function chapo()
    {
        return $this->chapo;
    }

    public function date()
    {
        if(!empty($this->date)){
            return $this->date->format('Y-m-d H:i:s');
        }
    }
    
    public function dateDisplay()
    {
        $datefmt = new IntlDateFormatter('fr_FR', NULL, NULL, NULL, NULL, 'EEEE dd LLLL à HH:mm:ss');
        if(!empty($this->date)){
            return $datefmt->format(new DateTime($this->date()));
        }
    }

    public function lastModificationDate()
    {
        if(!empty($this->lastModificationDate)){
            return $this->lastModificationDate->format('Y-m-d H:i:s');
        }
    }
    
    public function lastModificationDateDisplay()
    {
        $datefmt = new IntlDateFormatter('fr_FR', NULL, NULL, NULL, NULL, 'EEEE dd LLLL à HH:mm:ss');
        if(!empty($this->lastModificationDate)){
            return $datefmt->format(new DateTime($this->lastModificationDate()));
        }
    }


    // Liste des setters
  
    public function setIdSubscriber($id_subscriber)
    {
        $id_subscriber = (int) $id_subscriber;

        if ($id_subscriber > 0)
        {
            $this->idSubscriber = $id_subscriber;
            
            return $this;
        }
        
        else
        {
            throw new Exception('Blog-post : Identifiant de l\'abonné incorrect');
        }
    }
  
    public function setIdBlogPost($id_b_post)
    {
        $id_b_post = (int) $id_b_post;

        if($id_b_post > 0)
        {
            $this->idBlogPost = $id_b_post;
            
            return $this;
        }
        
        else
        {
            throw new Exception('Blog-post : Identifiant du blog-post incorrect');
        }
    }
  
    public function setContent($content)
    {
        $chaine = trim($content);
            
        if (!empty($chaine) && is_string($chaine))
        {
            $this->content = $chaine;

            return $this;
        }
        
        else
        {
            throw new Exception('Blog-post : Contenu incorrect');
        }
    }
    
    public function setTitle($title)
    {
        $chaine = trim($title);
            
        if (!empty($chaine) && is_string($chaine))
        {
            $this->title = $chaine;

            return $this;
        }
        
        else
        {
            throw new Exception('Blog-post : Titre non conforme');
        }
    }
  
    public function setChapo($chapo)
    {
        $chaine = trim($chapo);
            
        if (!empty($chaine) && is_string($chaine))
        {
            $this->chapo = $chapo;

            return $this;
        }
        
        else
        {
            throw new Exception('Blog-post : Chapo non conforme');
        }
    }
  
    //Setter non fonctionnel à refaire par REGEX
    public function setDate($date, $format = 'Y-m-d H:i:s')
    {     
        if(is_a($date, 'DateTime')){

            $this->date=$date;
            
            return $this;
        }

        else{
            $this->date=DateTime::createFromFormat($format, $date);
            
            return $this;
        }
    }
  
    
    //Setter non fonctionnel à refaire par REGEX
    public function setLastModificationDate($date, $format = 'Y-m-d H:i:s')
    {     
        if(is_a($date, 'DateTime')){

            $this->lastModificationDate=$date;
            
            return $this;
        }

        else{
            $this->lastModificationDate=DateTime::createFromFormat($format, $date);
            
            return $this;
        }
    }
        
    //Getter list for other class
    //Récupération des FK 
    public function getSubscriber()
    {
        $SubscriberManager = new SubscriberManager();
        $Subscriber = $SubscriberManager->read($this->idSubscriber);
        return $Subscriber;
    }
    
    public function display(){
        //Affiche les données du BP
        echo '<b>IdSubscriber : </b>'.$this->idSubscriber().'<br />';
        echo '<b>Title : </b>'.htmlspecialchars($this->title()).'<br />';
        echo '<b>Chapo : </b>'.htmlspecialchars($this->chapo()).'<br />';
        echo '<b>IdBlogpost : </b>'.$this->idBlogPost().'<br />';
        echo '<b>Content : </b>'.htmlspecialchars($this->content()).'<br />';
        echo '<b>Date : </b>'.$this->date().'<br />';
        echo '<b>LastModificationDate : </b>'.$this->lastModificationDate().'<br />';
    }
    
    
    //Méthode getcomments()
    public function getComments()
    {
        $CommentManager = new CommentManager();
        $listComments = $CommentManager->findFromIdBlogPost($this->idBlogPost());
        return $listComments;
    }
    
    public function getCommentsValidate()
    {
        $CommentManager = new CommentManager();
        $listComments = $CommentManager->findFromIdBlogPostForValidateComments($this->idBlogPost());
        return $listComments;
    }
}