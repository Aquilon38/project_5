<?php
    class Comment extends AbstractEntity{
    
        private $idSubscriber;
        private $idComment;
        private $content;
        private $idBlogPost;
        private $idState;
        private $date; 
        

        // Liste des getters

        public function idSubscriber()
        {
            return $this->idSubscriber;
        }

        public function idComment()
        {
            return $this->idComment;
        }

        public function content()
        {
            return $this->content;
        }
        
        public function idBlogPost() 
        {
            return $this->idBlogPost;
        }

        public function idState()
        {
            return $this->idState;
        }

        public function date()
        {
            if(!empty($this->date)){
                return $this->date->format('Y-m-d H:i:s');
            }
        }

        public function dateDisplay()
        {
            $datefmt = new IntlDateFormatter('fr_FR', NULL, NULL, NULL, NULL, 'EEEE dd LLLL à HH:mm:ss');
            if(!empty($this->date)){
                return $datefmt->format(new DateTime($this->date()));
            }
        }


        // Liste des setters

        public function setIdSubscriber($id)
        {
            $id = (int) $id;

            if ($id > 0)
            {
                $this->idSubscriber = $id;
                
                return $this;
            }
            
            else
            {
                throw new Exception('Comentaire : Identifiant de l\'abonné incorrect');
            }
        }

        public function setIdComment($comment)
        {
            $comment = (int) $comment;

            if ($comment > 0)
            {
                $this->idComment = $comment;
                
                return $this;
            }
            
            else
            {
                throw new Exception('Commentaire : Identifiant du commentaire incorrect');
            }
        }
        

        public function setContent($content)
        {
            $chaine = trim($content);
            
            if (!empty($chaine) && is_string($chaine))
            {
                $this->content = $content;
                
                return $this;
            }
            
            else
            {
                throw new Exception('Commentaire : Contenu non-conforme');
            }
        }
        
        public function setIdBlogPost($id)
        {
            $id = (int) $id;

            if ($id > 0)
            {
                $this->idBlogPost = $id;
                
                return $this;
            }
            
            else
            {
                throw new Exception('Commentaire : Identifiant du blogpost incorrect');
            }
        }

        public function setIdState($idState)
        {
            $idState = (int) $idState;
            
            if ($idState===1||$idState===2||$idState===3)
            {
                $this->idState = $idState;
                
                return $this;
            }
            
            else
            {
                throw new Exception('Commentaire : Statut de validation incorrect');
            }
        }

        public function setDate($date, $format = 'Y-m-d H:i:s')
        {        

            if(is_a($date, 'DateTime')){
                
                $this->date=$date;
            }
            
            else{
                $this->date=DateTime::createFromFormat($format, $date);
            }
            
//            else
//            {
//                throw new Exception('Commentaire : Date non conforme');
//            }
        }  
        
        public function display()
        {
            //Affiche les données du comment
            echo '<b>IdSubscriber : </b>'.$this->idSubscriber().'<br />';
            echo '<b>IdComment : </b>'.$this->idComment().'<br />';
            echo '<b>Content : </b>'.htmlspecialchars($this->content()).'<br />';
            echo '<b>IdState : </b>'.$this->idState().'<br />';
            echo '<b>Date : </b>'.$this->date().'<br />';
        }
        
        //FK entre comment et subscriber
        public function setSubscriber($Subscriber)
        {
            $this->SetIdSubscriber($Subscriber->idSubscriber());
            return $this;
        }
        
        
        public function getSubscriber()
        {
            $id = $this->idSubscriber();
            $subscriberManager = new SubscriberManager();
            return $subscriberManager->read($id);
        }
        
        
        public function getCommentStateName()
        {
            $stateCommentManager = new StateCommentManager();
            $nameState = $stateCommentManager->read($this->idState())->nameState();
            return $nameState;           
        }
        
    }