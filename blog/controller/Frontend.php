<?php

class Frontend{
    
    public function header()
    {
        $this->headerDisplayCondition();
    }
    
    public function headerDisplayCondition()
    {
        
        if(isset($_SESSION)&&!empty($_SESSION['connected_user'])){
            
            $subscriberManager = new SubscriberManager();
            $user = $subscriberManager->read($_SESSION['connected_user']);

            if($user->type()==1){
                require'view/frontend/headerSubscriber.php';
            }

            if($user->type()==2){
                require'view/backend/headerAdministrator.php';
            }
            
        }
        

        else {      
            //ajout des éléments html
            require 'view/frontend/addingConnexionElement.php';
        }
        
        if(isset($user)&&$user->type()==2){
            require('view/backend/BEheader.php');
        }
        
        else{
        require('view/common/header.php');
        }
    }

    public function home()
    {
        $_SESSION['accueilActive']='';
        require('view/frontend/homeView.php');
    }
    
    public function contact()
    {
        $_SESSION['contactActive']='';
        require('view/frontend/homeView.php');
    }

    public function recording()
    {
        $_SESSION['registerActive']='';
        require('view/frontend/addSubscriberView.php');
    }

    public function connection()
    {
        $_SESSION['connectionActive']='';
        require('view/frontend/connectionView.php');
    }
    
    public function connectionWays()
    {
        $mail = $_POST['mail'];
        $password = $_POST['password'];
        try{
            $subscriber = new Subscriber;
            $subscriber->setMail($mail);

            //Création d'un objet subscriber à partir de l'adresse mail
            $subscriberManager = new SubscriberManager();
            $accountExist = ($subscriberManager->readByMail($subscriber->mail()));

            //Si le compte existe, vérification du mot de passe
            if(!empty($accountExist)){

                $hash = $accountExist->password();
                $result = $subscriber->checkPassWord($password, $hash);

                //Les password concordent
                if($result === true){
                    $_SESSION['connected_user']=$accountExist->idSubscriber();
                    $this->home();
                }
                else{
                    throw new Exception('Le mot de passe est invalide');
                }
            }
            else{
                throw new Exception('Cette adresse mail n\'existe pas');
            }
        }
        catch(Exception $ex){
            $_SESSION['errors'][]=$ex->getMessage();
            $this->connection();
        }
    }
    
    public function disconnection()
    {
        unset($_SESSION['connected_user']);
        $this->home();
    }

    public function blogPosts()
    {
        try{
            //Partie modèle
            $blogPostManager = new BlogPostManager();
            $listBlogPost = $blogPostManager->findAll('lastModificationDate', 'DESC');
            $subscriberManager = new SubscriberManager();
            if(isset($_SESSION['connected_user'])){
                $user = $subscriberManager->read($_SESSION['connected_user']);
            }
        }
        catch(Exception $ex){
            $_SESSION['errors'][]=$ex->getMessage();
        }
      
        //partie vue
        require('view/frontend/blogPostsView.php');
    }
    
    public function blogPost()
    {
        //Modèle
        $blogPostManager = new BlogPostManager();
        $subscriberManager = new SubscriberManager();
        $blogpost = $blogPostManager->read($_GET['blogpost']);
        $comments = $blogpost->getCommentsValidate();
        
        //identification de l'administrateur (type == 2)
        $connectedUser = isset($_SESSION['connected_user']) ? $_SESSION['connected_user'] : NULL;
        $subscriber = $subscriberManager->read($connectedUser);
        
        if(!empty($subscriber)){
            $identityType = $subscriber->type();
        }
        
        else{
            $identifyType = -1;
        }
        
        //Vue
        require('view/frontend/displayBpView.php');
    }
    
    
    public function comment()
    {
        //Condition d'accés - si session activé
        if(isset($_SESSION)&&!empty($_SESSION['connected_user'])){
            //Condition d'accés - si jeton est le même que celui de la session
            if(isset($_SESSION['token'])&&$_SESSION['token']==$_POST['jeton']){
            
                try{
                $comment = new Comment();
                $commentManager = new CommentManager();
                $comment->setIdBlogPost($_GET['blogpost'])
                        ->setIdSubscriber($_SESSION['connected_user'])
                        ->setContent($_POST['message']);

                $commentManager->add($comment);
                $_SESSION['success'][] = "Merci ! <br /> Votre commentaire est en cours de validation.";
                }

                catch(Exception $ex) {
                    $_SESSION['errors'][] = $ex->getMessage();
                }
            }
        }
        
        //retour au bloc post avec message de traitement du commentaire
        $this->blogpost(); 
    }
    
    public function deleteComment()
    {
        if(isset($_SESSION)&&!empty($_SESSION['connected_user'])){
            //Suppression du commentaire dans BDD
            $commentManager = new CommentManager();
        
            try {
                $comment = $commentManager->read($_POST['deleteIdComment']);
                $comment->setIdComment($_POST['deleteIdComment']);
                $commentManager->delete($comment);
                $_SESSION['success'][] = 'Le commentaire a été éffacé';
            } 
            
            catch (Exception $ex) {
                $_SESSION['errors'][] = $ex->getMessage();
            }
            
            $this->blogPost();
        }
    }

    public function addSubscriber()
    {
        require('view/frontend/addSubscriberView.php');
    }

    public function disableSubscriber()
    {
        require('view/frontend/disableSubscriberView.php');
    }
    
    public function mail()
    {
        if(isset($_SESSION['token'])&&$_SESSION['token']==$_POST['jeton']){
            try{
                $mail = new EMail();
                $mail->setMail($_POST['mail'])
                     ->setFirstName($_POST['firstName'])
                     ->setLastName($_POST['lastName'])
                     ->setContent($_POST['message']);
                
                require 'view/backend/templateMail.php';
                // Envoi
                mail($to, $subject, $message, implode("\r\n", $headers));
                $_SESSION['success'][]="Merci ! <br /> Votre e-mail sera traîté dans les plus courts délais";
                
                //Effacement des données POST
                unset($_POST);
                unset($_SESSION['token']);
                }

                catch(Exception $ex){
                    $_SESSION['errors'][]=$ex->getMessage();
            }
        }
                //Retour sur la page home
                $this->home();
    }
    
    public function submitRegisterWays()
    {
        if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$#', $_POST['password'])){
            if($_POST['password']==$_POST['confirmationPassword']){
                $this->submitRegister();
            }

            else{
                $_SESSION['errors'][]="Les mots de passe ne concordent pas";
                $this->recording();
            }
        }

        else{
            //Envoi sur la page addSubscriber pour correction mot de passe
            $_SESSION['errors'][]="Votre mot de passe doit contenir au moins 8 caractères <br /> Dont au minimum : <br /> 1 caractère majuscule <br /> 1 caractère minuscule <br /> 1 chiffre <br /> 1 caractère spécial";
            $this->recording();
        }
    }
    
    public function submitRegister()
    {
        //Création de l'objet subscriber et ajout dans bdd
        $subscriber=new Subscriber();
        $subscriberManager=new SubscriberManager();
        
        if(isset($_SESSION['token'])&&$_SESSION['token']==$_POST['jeton']){
            try{
            $subscriber->setFirstName($_POST['firstName'])
                       ->setLastName($_POST['lastName'])
                       ->setMail($_POST['mail'])
                       ->setType(1);
            $password = $subscriber->passwordToHash($_POST['password']);
            $subscriber->setPassword($password);

            $subscriberManager->add($subscriber);
            
            //Création de l'objet mail, et hydratation
            $mail = new EMail();
            $mail->setMail($_POST['mail'])
                 ->setFirstName($_POST['firstName'])
                 ->setLastName($_POST['lastName']);
            
            //Message de bienvenue, envoi du mail de confirmation pour première connexion

            require 'view/backend/templateMail.php';
            // Envoi
            mail($to, $subject, $message, implode("\r\n", $headers));

            $_SESSION['success'][]="Bienvenue, <br />Un mail de confirmation a été envoyé sur votre adresse mail. <br />Bonne navigation !";
            
            //Affichage de la page d'accueil
            $this->home();
            }
        
            catch(Exception $ex){
                $_SESSION['errors'][]= $ex->getMessage();
                $this->recording();
            }
        }
        else{
            $this->home();
        }
    }

    public function displayButtonDeleteCommentaire() 
    {
        require ('view/frontend/displayButtonDeleteCommentaire.php');
    }
    
    public function errorsNotification()
    {
        if(isset($_SESSION['errors'])){
            require 'view/common/notification.php';
            unset($_SESSION['errors']);
        }
    }
    
    public function success()
    {
        if(isset($_SESSION['success'])){
            require 'view/common/notification.php';
            unset($_SESSION['success']);
        }
    }
    
    public function token()
    {
        $random = random_bytes(140);
        return (bin2hex($random));
    }
}

