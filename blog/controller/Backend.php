<?php


class Backend extends Frontend{
    
    public function moderationView()
    {
        $_SESSION['moderationActive']='';
        require('view/backend/moderationView.php');
    }
    
    public function masterBlogPost() 
    {
        require('view/backend/masterBlogPostView.php');
    }
    
    public function addBlogPost()
    {
        $_SESSION['newBlogPostActive']='';
        require('view/backend/addBlogPostView.php');
    }
    
    public function addBlogPostProcess()
    {   
        if(isset($_SESSION['token'])&&$_SESSION['token']==$_POST['jeton']){
            try{
                
            //model
            $blogpostManager = new BlogPostManager();
            $blogpost = new BlogPost();

            $blogpost->setIdSubscriber($_SESSION['connected_user']);
            $blogpost->setTitle($_POST['titleBlogPost'])
                     ->setChapo($_POST['chapoBlogPost'])
                     ->setContent($_POST['contentBlogPost'])
                     ->setDate(date('Y-m-d H:i:s'));

            $blogpostManager->add($blogpost);

            $_SESSION['success'][]='Nouveau Blog Post enregistré avec succés';
                unset($_SESSION['token']);
                unset($_POST);
            }

            catch(Exception $ex){
                $_SESSION['errors'][]=$ex->getMessage();
            }

            //View
            //Retour à la page d'ajout du blogpost
            $this->addBlogPost();

        }
        
        else{
            $this->addBlogPost();
        }
        
    }
    
    public function modifyBlogPost()
    {
        try{
            $blogpostManager = new BlogPostManager();
            if(isset($_SESSION['modifyblogpost'])){
                $_GET['modifyblogpost']=$_SESSION['modifyblogpost'];
            }
            unset($_SESSION['modifyblogpost']);
            $blogpost = $blogpostManager->read($_GET['modifyblogpost']);
            $titleBlogPost = $blogpost->title();
            $chapoBlogPost = $blogpost->chapo();
            $contentBlogPost = $blogpost->content();
            $idBlogPost = $blogpost->idBlogPost();
            $idSubscriber = $blogpost->idSubscriber();
        }
        
        
        catch (Exception $ex) {
            $_SESSION['errors'][]=$ex->getMessage();
        }
        
        require('view/backend/modifyBlogPostView.php');
    }
    
    
    public function modifyProcessBlogPost()
    {
        try{
            $blogpostManager = new BlogPostManager();
            $blogpost = new BlogPost();
            $blogpost = $blogpostManager->read($_POST['idBlogpost']);


            $blogpost->setTitle($_POST['modifyTitleBlogPost'])
                     ->setChapo($_POST['modifyChapoBlogPost'])
                     ->setContent($_POST['modifyContentBlogPost'])
                     ->setLastModificationDate(date('Y-m-d H:i:s'));


            $blogpostManager->modify($blogpost);
            $_SESSION['success'][]='Le blog post a bien été modifié';
            
            $this->blogPosts();
        }
        
        catch (Exception $ex) {
            $_SESSION['errors'][]=$ex->getMessage();
            $_SESSION['modifyblogpost']=$_POST['idBlogpost'];
            $this->modifyBlogPost();
        }
    }

    public function deleteBlogPost()
    {
        if($this->rightOfModeration()==true){
            try{
                //Suppression des commentaires ayant pour PK idBlogPost
                $blogpost = new BlogPost();
                $blogpostManager = new BlogPostManager();
                $blogpost = $blogpostManager->read($_GET['deleteblogpost']);
                $comments=$blogpost->getComments();

                $commentManager = new CommentManager();

                foreach($comments as $comment)
                {
                    $commentManager->delete($comment);
                }

                //Suppression du blogpost concerné
                $blogpost->setIdBlogPost($_GET['deleteblogpost']);
                $blogpostManager = new BlogPostManager();
                $blogpostManager->delete($blogpost);
                $_SESSION['success'][]='Le Blog Post a bien été supprimé';
            }

            catch(Exception $ex) {
                $_SESSION['errors'][] = $ex->getMessage();
            }

            //view
            $this->blogPosts();
        }
    }
    
    public function validationComments() {
        
        //model
        //Liste des commentaire en cours de validation
        $commentManager = new CommentManager();
        $comments = $commentManager->commentValidationCourse();
        
        //view
        require('view/backend/validationComments.php');
    }
    
    public function validationComment(){
        //model
        if($this->rightOfModeration()==true){
            try{
                $commentManager = new CommentManager();
                $comment = new Comment();
                $comment = $commentManager->read($_GET['validationComment']);
                $comment->setIdState(1);
                $commentManager->modify($comment);
                $_SESSION['success'][]="Le commentaire a bien été validé";
            }
            
            catch (Exception $ex){
                $_SESSION['errors'][]=$ex->getMessage();
            }

        //view
             $this->validationComments(); 
        }
    }
    
    public function refuseComment() {
        
        try{
        //model
        $commentManager = new CommentManager();
        $comment = new Comment();
        $comment = $commentManager->read($_GET['refuseComment']);
        $comment->setIdState(3);
        $commentManager->modify($comment);
        $_SESSION['success'][]="Le commentaire a bien été refusé";
        }
        
        catch (Exception $ex){
            $_SESSION['errors'][]=$ex->getMessage();
        }
        //view
        $this->validationComments(); 
    }
    
    public function deleteAccount() {
        
        //model
        $subscribers = new Subscriber();
        $subscriberManager = new SubscriberManager();
        $subscribers = $subscriberManager->findAll();
        
        //view
        require ('view/backend/deleteAccountView.php');
    }
    
    public function deleteSubscriber(){
        
        if($this->rightOfModeration()==true){
            try{
            $subscriberManager = new SubscriberManager();
            $subscriber = new Subscriber();
            $subscriber->setIdSubscriber($_GET['deleteSubscriber']);
            $commentManager = new CommentManager();
            
            //Suppression des commentaires lié au subscriber
            $comments = $subscriber->getComments();
            
            if(!empty($comments)){

                foreach($comments as $comment){
                    $commentManager->delete($comment);
                }
            }
            
            //Suppression du blogpost lié à l'idSubscriber
            $blogpostManager=new BlogPostManager;
            $blogposts = $subscriber->getBlogPosts();
            
            if(!empty($blogposts)){
                foreach($blogposts as $blogpost){
                    $blogpostManager->delete($blogpost);
                }
            }
            
                $subscriberManager->delete($subscriber);

                $_SESSION['success'][]="L'adhérent a bien été supprimé";
            }
            
            catch (Exception $ex){
                $_SESSION['errors'][]=$ex->getMessage();
            }
            
            $this->deleteAccount();
        }
        
        else{
            $this->home();
        }
    }
    
    
    //méthode de sécurité
    public function rightOfModeration()
    {
        $subscriber = new Subscriber();
        $subscriberManager = new SubscriberManager();
        
        if(isset($_SESSION['connected_user'])||!empty($_SESSION['connected_user'])){
            $subscriber = $subscriberManager->read($_SESSION['connected_user']);
            if($subscriber->type()==2){
                return true;
            }
            
            else{
                return false;
            }
        }
        
        else{
            return false;
        }
    }
}
