<?php

class EMail{
    
    private $mail;
    private $firstName;
    private $lastName;
    private $content;

    
  
    // Liste des getters

    public function mail()
    {
        return $this->mail;
    }

    public function firstName()
    {
        return $this->firstName;
    }

    public function lastName()
    {
        return $this->lastName;
    }
    
    public function content()
    {
        return $this->content;
    }


    // Liste des setters
  
    public function setMail($mail)
    {        
        if (preg_match("#^[\w][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$#", $mail))
        {
            $this->mail = $mail;
            return $this;
        }
        
        else
        {
            throw new Exception('Adresse mail non valide');
        }
    }
    
    
    public function setFirstName($firstName)
    {
        if (is_string($firstName) && strlen($firstName) <= 30 && strlen($firstName) >= 1 && !empty(trim($firstName)))
        {
            $this->firstName = $firstName;
            return $this;
        }
        
        else
        {
            throw new Exception('La rédaction de votre prénom n\'est pas conforme');
        }
    }
    
    public function setLastName($lastName)
    {
        if (is_string($lastName) && strlen($lastName) <= 30 && strlen($lastName) >= 1 && !empty(trim($lastName)))
        {
            $this->lastName = $lastName;
            return $this;
        }
        
        else
        {
            throw new Exception('La rédaction de votre nom n\'est pas conforme');
        }
    }
    
    
    public function setContent($content)
        {
            $chaine = trim($content);
            
            if (!empty($chaine) && is_string($chaine))
            {
                $this->content = $content;
                
                return $this;
            }
            
            else
            {
                throw new Exception('Contenu non-conforme');
            }
        }
}