<?php
session_start();
require('config/config.php');

function chargerClasse($classe){

    if(file_exists('controller/'.$classe.'.php'))
    {
        require 'controller/'.$classe.'.php';
    } else if(file_exists('model/'.$classe.'.php')){
        require 'model/'.$classe.'.php';
    } else if(file_exists('utils/'.$classe.'.php')){
        require 'utils/'.$classe.'.php';
    }
    
}

spl_autoload_register('chargerClasse');

//Instanciation de l'objet frontend
$frontend = new Frontend();

$backend = new Backend();


//Menu d'accés rapide aux pages blogpost, contact, connection, register

if (isset($_GET['main'])) {
    
    switch($_GET['main']){
        
        case 'blogposts':
            $frontend->blogPosts();
            break;
        
        case 'contact':
            $frontend->contact();
            break;
    
        case 'connection':
            $frontend->connection();
            break;
        
        case 'disconnection':
            $frontend->disconnection();
            break;
    
        case 'register':
            $frontend->recording();
            break;
        
        default: 
            $frontend->home();
        
    }
}

elseif (isset($_GET['admin'])){
    
    switch($_GET['admin']){
        
        case 'moderation':
            $backend->moderationView();
            break;
        
        case 'masterBlogPost':
            $backend->masterBlogPost();
            break;
        
        case 'deleteBlogPost':
            $backend->deleteBlogPost();
            break;
        
        case 'addBlogPost':
            $backend->addBlogPost();
            break;
        
        case 'modifyBlogPost':
            $backend->modifyBlogPost();
            break;
        
        case 'validationComments':
            $backend->validationComments();
            break;
        
        case 'deleteAccount';
            $backend->deleteAccount();
            break;
        
        default:
            $frontend->home();
    }
}

elseif (isset($_GET['deleteSubscriber'])){
    $backend->deleteSubscriber();
}

elseif (isset($_GET['validationComment'])){
    $backend->validationComment();
}

elseif (isset($_GET['refuseComment'])){
    $backend->refuseComment();
}

//Page d'ajout d'un blogpost
elseif (isset($_POST['newBlogPost'])){
    $backend->addBlogPostProcess();
}

elseif(isset($_POST['comment'])){
    $frontend->comment();
}

elseif(isset($_POST['deleteComment'])){
    $frontend->deleteComment();
}

elseif(isset($_GET['blogpost'])){
    $frontend->blogPost();
}

//Lien de modification du blogpost
elseif(isset($_GET['modifyblogpost'])){
    $backend->modifyBlogPost();
}

//BlogBostModifié
elseif(isset($_POST['modifyProcessBlogPost'])){
    $backend->modifyProcessBlogPost();
}

elseif(isset($_GET['deleteblogpost'])){
    $backend->deleteBlogPost();
}

//Page contact / envoi d'un email
elseif(isset($_POST['contact'])){
    $frontend->mail();
}

//Page ajout d'un compte abonné
elseif (isset($_POST['submitRegister'])){
    $frontend->submitRegisterWays();
}
    
//Page connection
elseif (isset($_POST['connection'])){
    $frontend->connectionWays();
}

//Page princpale
else {
    $frontend->home();
}