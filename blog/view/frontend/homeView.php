<?php
    $title= "Accueil";
?>

<?php
    ob_start();
    //Affichage notification commentaire
    $this->errorsNotification();
    $this->success();
?>

    <!-- Content header-->
  
    <!--  Header inclus à part-->

    <!-- Content section -->
    <section class="py-5 behaviorSectionHome">
        <div class="behaviorContainer">
            <h1>Jonathan Madelon</h1>
            <p class="lead">De gustibus et coloribus non disputandum</p>
        </div>
        <div class="behaviorLogos">
            <a href="https://bitbucket.org/Aquilon38/project_5/commits/"><img class="logoHeight" src="public/images/bitbucket.png" title="Confection de ce BlogPost" alt="BitBucket"></a>
            <a href="public/images/CV.pdf"><img class="logoHeight" src="public/images/cv.png" title="Télécharger le CV de Jonathan Madelon" alt="CV"></a>
            <a href="https://twitter.com/?lang=fr"><img class="logoHeight" src="public/images/twitter.png" alt="Twitter"title="Twitter"></a>
        </div>
    </section>
  
        <!-- Image Section - set the background image for the header in the line below -->
    <section class="py-5 bg-image-full" style="background-image: url('https://unsplash.it/1900/1080?image=1081');">
        <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
        <div style="height: 70px;"></div>
    </section>

    <section id="formulaire_contact">
        <h1 class="text-center moonglade py-5">ME CONTACTER</h1>
        <form action="index.php" method="post" class='text-center mb-4'>
            <div class="form-row ml-4 mr-4">
                <div class="offset-2 offset-sm-3 offset-xl-0 col-sm-6 col-8 mb-3">
                    <label for="firstName">Votre prénom</label>
                    <input required type="text" class="form-control" id="firstName" name="firstName" <?php if(isset($_SESSION['token'])&&(isset($_POST['jeton']))&&$_SESSION['token']==$_POST['jeton']){ if(isset($_POST['firstName'])){echo'value='.$_POST['firstName'];} else{ echo'placeholder="Prénom"';}}?>>
                </div>
                <div class="offset-2 offset-sm-3 offset-xl-0 col-sm-6 col-8 mb-3">
                    <label for="lastName">Votre nom</label>
                    <input required type="text" class="form-control" id="lastName" name="lastName" <?php if(isset($_SESSION['token'])&&(isset($_POST['jeton']))&&$_SESSION['token']==$_POST['jeton']){ if(isset($_POST['lastName'])){echo'value='.$_POST['lastName'];} else{ echo'placeholder="Nom"';}}?>>
                </div>
                <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
                    <label for="mail">Votre adresse mail</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend2">@</span>
                        </div>
                        <input required type="text" aria-describedby="inputGroupPrepend2" class="form-control" name='mail' id="mail" <?php $placeholderMail='placeholder="Mail"'; if(isset($_SESSION['token'])&&(isset($_POST['jeton']))&&$_SESSION['token']==$_POST['jeton']){ if(isset($_POST['mail'])){echo'value='.$_POST['mail'];} else{ echo'placeholder="Mail"';}}?>>
                    </div>
                </div>
            </div>
            <div class="form-group ml-4 mr-4">
                <label for="message">Votre message</label>
                <textarea class="form-control mb-3" id="message" name="message" rows="3" required><?php if(isset($_SESSION['token'])&&(isset($_POST['jeton']))&&$_SESSION['token']==$_POST['jeton']){ if(isset($_POST['message'])){echo $_POST['message'];}}?></textarea>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck" required>
                    <label class="form-check-label" for="gridCheck">
                        Recueil des données temporaire pour prise de contact et réponse seul.
                    </label>
                </div>
            </div>
            <?php
                require 'utils/token.php';
            ?>
            <input type="hidden" name="jeton" value=<?=$jeton?>>
            <button class="btn btn-outline-dark" name="contact" type="submit">Envoyer le message</button>
        </form>
    </section>

<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>