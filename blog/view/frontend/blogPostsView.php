<?php

    $title= "Enregistrement";
?>



<?php
    ob_start();
?>

<!--Header-->

<!--Corps-->

<!--Section-->

    <section class="text-center py-5 row">
            <h1 class="py-5 offset-1 col-10 elegant">LES BLOG POSTS</h1>
            <?php
            if(isset($_SESSION['errors'])||isset($_SESSION['success'])){
                echo '<div class="offset-1 col-10">';
                $this->success();
                $this->errorsNotification();
                echo '</div>';
            }
            ?>

        <?php
            if($listBlogPost!=NULL){
                foreach ($listBlogPost as $key => $blogpost){?>

                    <div class="MarginTopBlogPosts frameBlogPosts offset-1 col-10 pb-3 pt-2">

                        <h2 class="moonglade"><?php echo htmlspecialchars($blogpost->title()); ?></h2>
                        <h3 class="trench"><?php echo htmlspecialchars($blogpost->chapo()); ?></h3>
                        <p><i><?php echo 'Posté le '.$blogpost->lastModificationDateDisplay(); ?></i></p>
                        <?php 
                            echo'<a class="btn btn-outline-dark" href="index.php?blogpost='.$blogpost->idBlogPost().'">Consulter</a>';

                        if(!empty($user) && $user->type()==2){
                            ?>
                            <a class="btn btn-outline-danger" href="index.php?deleteblogpost=<?=$blogpost->idBlogPost()?>" onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce blogpost ? ')">Supprimer</a>
                            <a class="btn btn-outline-warning" href="index.php?modifyblogpost=<?=$blogpost->idBlogPost()?>">Modifier</a>
                        <?php
                        }
                        ?>

                    </div>
                    <br />
            
        <?php 
                }
            }
        ?>
    </section>
        


<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>