<?php
    $title= "Invalider un abonné";
?>

<?php
    ob_start();
?>

<!--Corps-->

<!-- Content header-->

<!--Section-->

<section class='text-center mt-4'>
    <p>Vous pourrez réactiver votre compte lors de votre prochaine connexion<br />Souhaitez-vous désactiver votre compte ? </p>
    <form method="post" action="index.php" class='mb-4 mt-4'>    
        <button class="btn" type="submit">Je désactive mon compte</button>
    </form>
</section>

<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>