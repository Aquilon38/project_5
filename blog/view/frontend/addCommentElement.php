<!--Affichage d'un ajout de commentaire-->
<form action="index.php?blogpost=<?=$_GET['blogpost']?>" method="post" class='text-center mb-4 mt-4'>
    <div class="form-group ml-4 mr-4">
        <label for="message"><b>Laisser un commentaire ? </b></label>
        <textarea class="form-control mb-3" id="message" name="message" rows="3" required></textarea>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck" required>
            <label class="form-check-label" for="gridCheck">
                Recueil des données temporaire pour prise de contact et réponse seul.
            </label>
        </div>
    </div>
    <?php
        require 'utils/token.php';
    ?>
    <input type="hidden" name="jeton" value=<?=$jeton?>>
    <button class="btn btn-outline-dark" name="comment" type="submit">Envoyer le commentaire</button>
</form>

