<?php
    $title= "Enregistrement";
?>

<?php
    ob_start();
?>

  <!-- Content header-->


<!-- Content section-->

<section>
    <?php
    //Affichage des notifications
    $this->errorsNotification();
    $this->success();
    ?>
    <form method="post" action="index.php" class='text-center behaviorConnection'>
       <div class="col-8 col-sm-6 offset-2 offset-sm-3 mt-5 mb-5">
            <label for="mail">Votre adresse mail</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend2">@</span>
                </div>
                <input type="text" class="form-control" name="mail" id="mail" placeholder="Username" aria-describedby="inputGroupPrepend2" required>
            </div>
        </div>

        <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-5">
            <label for="password">Votre mot de passe</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Mot de passe" required>
        </div>
        
        <button class="btn btn-outline-dark" name="connection" type="submit">Se connecter</button>
        
    </form>
</section>

<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>