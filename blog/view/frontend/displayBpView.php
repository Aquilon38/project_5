<?php
    $title= "Lire un Blogspot";
?>



<?php
    ob_start();
?>

<!--Corps-->

<!--Section-->

<section class="py-5 sizeMainAreaBlogPost d-flex text-center flex-column align-items-center justify-content-center row">

    <div class="frameBlogPosts col-10 py-4">
        <?php
        //Affichage notification commentaire
        $this->errorsNotification();
        $this->success();
        ?>

        <h2 class="moonglade"><?php echo htmlspecialchars($blogpost->title()); ?></h2>
        <h3 class="trench"><?php echo htmlspecialchars($blogpost->chapo()); ?></h3>
        <p><i><?php echo 'Posté le '.$blogpost->lastModificationDateDisplay(); ?></i></p>
        <br />
        <p><?php echo '<b>'.htmlspecialchars($blogpost->content()).'</b>'; ?></p>
    </div>

    <!--Si subscriber en ligne affichage élément d'ajout de commentaire-->
    <?php         
        if(isset($_SESSION)&&!empty($_SESSION['connected_user'])){
        require 'view/frontend/addCommentElement.php';
    }
    
        if($comments!=NULL){
            echo'<div class="mt-5 col-10"><h1 class="moonglade">COMMENTAIRES</h1>';
        
            foreach ($comments as $comment){

                $comment->idSubscriber();

                echo'<p class="mt-5 mb-2 pt-4 pb-4 frameCommentBlogPost col-12">';

                    echo '<b>'.htmlspecialchars($comment->content()) .'</b><br /><br />';
                    echo '<i>Déposé par <b>'.htmlspecialchars($comment->getSubscriber()->getFullName());
                            if(!empty($_SESSION['connected_user'])){
                                if($identityType==2){
                                    echo ' <b> ('.$comment->getSubscriber()->mail().') </b>';
                                }
                            }
                            echo '</b> le '.$comment->dateDisplay().'</i>';

                echo'</p>';    

                if(!empty($_SESSION['connected_user'])){
                    if($_SESSION['connected_user'] == $comment->idSubscriber()||$identityType==2){
                        require 'view/frontend/displayButtonDeleteCommentaire.php';
                    }
                }
            }
            
            echo'</div>';
        }

    ?>

</section>

<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>