<?php
    $title= "Enregistrement";
?>

<?php
    ob_start();
?>

<!-- Content header-->

<!--Corps-->
        
<section class="py-4">
    <?php
        if(isset($_SESSION['errors'])||isset($_SESSION['success'])){
            echo '<div class="offset-1 col-10">';
            $this->success();
            $this->errorsNotification();
            echo '</div>';
        }
    ?>

    <form action="index.php" method="post" class='text-center mb-4'>
        <div class="form-row ml-4 mr-4">
            <div class="offset-2 offset-sm-3 offset-xl-1 col-xl-4 col-sm-6 col-8 mb-3">
                <label for="firstName">Votre prénom</label>
                <input type="text" class="form-control" id="firstName" name="firstName" <?php if(isset($_POST['firstName'])){echo'value='.$_POST['firstName'];} else{ echo'placeholder="Prénom"';}?> required>
            </div>
            <div class="offset-2 offset-sm-3 offset-xl-2 col-xl-4 col-sm-6 col-8 mb-3">
                <label for="lastName">Votre nom</label>
                <input type="text" class="form-control" id="lastName" name="lastName" <?php if(isset($_POST['lastName'])){echo'value='.$_POST['lastName'];} else{ echo'placeholder="Nom"';}?> required>
            </div>
            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-4">
                <label for="mail">Votre adresse mail</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend2">@</span>
                    </div>
                    <input type="text" class="form-control" name="mail" id="mail" <?php if(isset($_POST['mail'])){echo'value='.$_POST['mail'];} else{ echo'placeholder="e-mail"';}?> aria-describedby="inputGroupPrepend2" required>
                </div>
            </div>

            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-4">
                <label for="password">Votre mot de passe</label>
                <input type="password" class="form-control" id="password" name="password" <?php if(isset($_POST['password'])){echo'value='.$_POST['password'];} else{ echo'placeholder="Mot de passe"';}?> required>
            </div>

            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-4">
                <label for="confirmationPassword">Confirmation de votre mot de passe</label>
                <input type="password" class="form-control" id="confirmationPassword" name="confirmationPassword" <?php if(isset($_POST['password'])){echo'value='.$_POST['password'];} else{ echo'placeholder="Confirmation du mot de passe"';}?> required>
            </div>

            <div class="col-8 col-sm-6 offset-2 offset-sm-3">
                <?php
                    require 'utils/token.php';
                ?>
                <input type="hidden" name="jeton" value=<?=$jeton?>>
                <button class="btn btn-outline-dark" name='submitRegister' type="submit">Confirmer</button>
            </div>
        </div>
    </form>
</section>

<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>