<?php
    $title= "Supprimer un Blogpost";
?>

<?php
    ob_start();
?>

<!--Corps-->

<!--header-->

<section>
    <form method="post" action="index.php" class='text-center mb-4 mt-4'>
        <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
            <label for="id_b_post">Numéro identifiant du Blogpost</label>
            <input type="text" class="form-control" name="id_b_post" id="id_b_post" placeholder="Numéro identifiant" required>
        </div>

        <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
            <label for="read">Vérifier le Blogpost avant de choisir de le supprimer</label>
            <input type="checkbox" class="form-control" name="read" id="read">
        </div>
        
        <button class="btn" type="submit">Confirmer</button>
        
    </form>
</section>

<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>