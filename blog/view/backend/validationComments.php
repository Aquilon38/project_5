<?php
    $title= "Liste des commentaires";

    ob_start();
    
echo'<section class=" py-4 behaviorValidationComments d-flex flex-column row">'
    . '<h1 class="moonglade text-center py-4 pb-5">VALIDATION DES COMMENTAIRES</h1>';

$this->success();

    
if ($comments != NULL){
    foreach ($comments as $comment) {
?>

    <div class="text-center offset-1 col-10 frameBlogPosts py-3 mb-4">
        <!--<p><?php // echo $comment->idComment();?></p>-->
        <p><?php echo 'De <b>'.$comment->getSubscriber()->mail().' </b><br /><b>Son commentaire : </b>'.htmlspecialchars($comment->content());?></p>    
        <p><?php echo '<i>le '.$comment->dateDisplay().'</i>';?></p>    
        <div class="d-flex flex-wrap justify-content-around offset-2 col-8">
            <p>
                <a class="btn btn-validationComments btn-outline-success" href="index.php?validationComment=<?=$comment->idComment()?>">Valider</a>
            </p>

            <p>
                <a class="btn btn-validationComments btn-outline-danger" href="index.php?refuseComment=<?=$comment->idComment()?>">Refuser</a>
            </p>
        </div>
    </div>
   
<?php
    }
}

else{
    $_SESSION['errors'][]="Il n'y a aucun commentaire en cours de validation";
    $this->errorsNotification();
}

echo '</section>';

    $content = ob_get_clean();
    require ("view/common/template.php");
?>