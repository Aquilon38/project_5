<?php
    $title= "Supprimer un compte";
?>

<?php
    ob_start();
?>

<!--Corps-->

<!--Header-->

<!--Section-->
<section class="py-5">
    <h1 class="text-center moonglade py-5">QUEL COMPTE VOULEZ-VOUS SUPPRIMER ? </h1>

<?php
    // Remonté des notifications
    $this->errorsNotification();
    $this->success();
    foreach($subscribers as $subscriber){
?>
    
        <div class="text-center mb-4 mt-4"> 
            <a class="btn btn-outline-danger" href="index.php?deleteSubscriber=<?=$subscriber->idSubscriber()?>" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cet adhérent ? ')"> 
            <?=
                $subscriber->firstName().' '.$subscriber->lastName().' <b>'.$subscriber->mail().'</b>';
            ?>
            </a>
        </div>


<?php
    }
?>
</section>
    
<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>