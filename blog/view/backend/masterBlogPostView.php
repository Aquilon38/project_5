<?php
    $title= "Menu BlogPost";
?>

    

<?php
    ob_start();
?>

  <!-- Content header-->
<!--  Header inclus à part-->

  <!-- Content section -->
  
    <section id="formulaire_contact" class="py-5">
        <div action="homeView.php" method="post" class='text-center mb-4 mt-4'>
            <div class="form-row ml-4 mr-4">
                <div class="offset-2 offset-sm-3 offset-xl-0 col-sm-6 col-8 mb-3">
                    <a class="btn" href="index.php?admin=addBlogPost">Ajouter un Blogpost</a>
                </div>
                <div class="offset-2 offset-sm-3 offset-xl-0 col-sm-6 col-8 mb-3">
                    <a class="btn" href="index.php?admin=modifyBlogPost">Modifier un Blogpost</a>
                </div>
                <div class="offset-2 offset-sm-3 offset-xl-0 col-sm-6 col-8 mb-3">
                    <a class="btn" href="index.php?admin=deleteBlogPost">Supprimer un Blogpost</a>
                </div>
        </div>
    </div>
  
<?php
    $content = ob_get_clean();
    require ("view/common/template.php");