<?php
    $title= "Nouveau Blogpost";
?>

<?php
    ob_start();
?>

<!--Corps-->

<!--Header-->

<!--Section-->
<section id="formulaire_contact" class="py-5">
    <h1 class="text-center moonglade py-5">NOUVEAU BLOG POST</h1>
    <?php
        $this->errorsNotification();
        $this->success();
    ?>
    <form action="index.php" method="post" class='text-center mb-4 mt-4'>
        <div class="form-row ml-4 mr-4">
            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
                <label for="validationDefault01">Titre</label>
                <input required type="text" class="form-control" id="validationDefault01" name='titleBlogPost' <?php if(isset($_POST['titleBlogPost'])){echo'value='.$_POST['titleBlogPost'];} else{ echo'placeholder="Titre"';}?>>
            </div>
            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
                <label for="validationDefault02">Chapo</label>
                <input required type="text" class="form-control" id="validationDefault02" name='chapoBlogPost' <?php if(isset($_POST['chapoBlogPost'])){echo'value='.$_POST['chapoBlogPost'];} else{ echo'placeholder="Chapo"';}?>>
            </div>
            <div class="col-10 offset-1 mb-3">
                <label for="validationDefault03">Contenu</label>
                <textarea required class="form-control" id="validationDefault03" name='contentBlogPost' <?php if(isset($_POST['contentBlogPost'])){echo'value='.$_POST['contentBlogPost'];} else{ echo'placeholder="Contenu"';}?> rows="5"><?php if(isset($_POST['contentBlogPost'])){echo $_POST['contentBlogPost'];}?></textarea>
            </div>
            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
                <!--Mise en place du jeton en hidden-->
                <?php   
                        $token=$this->token(); 
                        $jeton=$token;
                        $_SESSION['token']=$token; 
                ?>
                <input type="hidden" name="jeton" value="<?=$jeton?>">
                <button class="btn btn-outline-dark" name="newBlogPost" type="submit">Confirmer</button>
            </div>
        </div>
    </form>
</section>

<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>