<?php
    $title= "Modifier un blogpost";
?>

<?php
    ob_start();
?>

<!--Corps-->

<!--Header-->

<!--Section-->

<section id="formulaire_contact" class="py-5">
    <?php
        $this->errorsNotification();
        $this->success();
    ?>
    <form action="index.php" method="post" class='text-center mb-4 mt-4'>
        <div class="form-row ml-4 mr-4">
            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
                <label for="validationDefault01">Titre</label>
                <input required type="text" class="form-control" id="validationDefault01" name='modifyTitleBlogPost' <?='value ="'.$titleBlogPost.'"'?>>
            </div>
            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
                <label for="validationDefault02">Chapo</label>
                <input required type="text" class="form-control" id="validationDefault02" name='modifyChapoBlogPost' <?='value ="'.$chapoBlogPost.'"'?>>
            </div>
            <div class="col-10 offset-1 mb-3">
                <label for="validationDefault03">Contenu</label>
                <textarea class="form-control" id="validationDefault03" name='modifyContentBlogPost' rows="5" required><?=$contentBlogPost?></textarea>
            </div>
            <div class="col-8 col-sm-6 offset-2 offset-sm-3 mb-3">
                <input type="hidden" name='idBlogpost' <?='value ="'.$idBlogPost.'"'?>>
                <input type="hidden" name='idSubscriber' <?='value ="'.$idSubscriber.'"'?>>
                <button class="btn btn-cinema" name="modifyProcessBlogPost" type="submit">Confirmer</button>
            </div>
        </div>
    </form>
</section>

<?php
    $content = ob_get_clean();
    require ("view/common/template.php");
?>