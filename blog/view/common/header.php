    <!-- Header - set the background image for the header in the line below -->
    <header class="py-5 bg-image-full" style="background-image: url('https://unsplash.it/1900/1080?image=1076'); height: 240px;">
      
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

            <div class="container">
                <a class="navbar-brand" href="index.php?main=blogposts">Blog Post</a>
                <?= $identifiant ?>

                <button class="navbar-toggler" id='main_menu' type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link <?php if(isset($_SESSION['accueilActive'])){ echo ' active'; unset($_SESSION['accueilActive']);}?>" href="index.php">Accueil
                            </a>
                        </li>
                        <li class="nav-item">
                            <?= $connection ?>
                        <?php if(!isset($_SESSION['connected_user'])){?>
                        <li class="nav-item">
                            <a class="nav-link <?php if(isset($_SESSION['registerActive'])){ echo ' active'; unset($_SESSION['registerActive']);}?>" href="index.php?main=register">S'enregistrer</a>
                        </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </nav>
      
    </header>