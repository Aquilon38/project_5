<?php

if(isset($_SESSION['errors'])){ 

    foreach($_SESSION['errors'] as $erreur){
    ?>
    <div class="alert alert-danger text-center">
        <p class="mt-2 mb-2">
            <b><?=$erreur?></b>
        </p>
    </div>

    <?php
    }
}

if(isset($_SESSION['success'])){
    foreach($_SESSION['success'] as $success){
    ?>
    <div class="alert alert-success text-center">
        <p class="mt-2 mb-2">
            <b><?=$success?></b>
        </p>
    </div>

    <?php
    }
}




